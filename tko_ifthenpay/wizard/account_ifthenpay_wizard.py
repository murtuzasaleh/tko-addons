from odoo import api, fields, models, _


class AccountIfthenpayWizard(models.TransientModel):
    _name = 'account.ifthenpay.wizard'
    _description = 'Account IfThenPay Wizard'

    @api.model
    def _default_start_datetime(self):
        return self.env['account.ifthenpay']._default_start_utc_datetime()

    start_datetime = fields.Datetime('Start', required=True, default=_default_start_datetime)
    end_datetime = fields.Datetime('End', required=True, default=lambda self: fields.Datetime.now())

    _sql_constraints = [
        ('end_datetime_greater', 'check(end_datetime >= start_datetime)',
         'Error! The end of synchronization cannot be set before the start.')
    ]

    def sync_payments(self):
        context = dict(self._context or {})
        accounts_ifthenpay = self.env['account.ifthenpay'].browse(context.get('active_ids'))
        for account_ifthenpay in accounts_ifthenpay:
            account_ifthenpay.sync_payments(start_utc_datetime=self.start_datetime,
                                            end_utc_datetime=self.end_datetime)
        return True
