import datetime
import json
import pytz
import requests
import xml.etree.ElementTree

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import AccessError, UserError

timezone_LX = pytz.timezone('Europe/Lisbon')
WebServiceMultibancoURL = 'https://www.ifthenpay.com/IfmbWS/WsIfmb.asmx/GetPaymentsJson'


class AccountIfthenpay(models.Model):
    _name = 'account.ifthenpay'
    _description = 'Account IfThenPay'

    CALLBACK_DTF = '%d-%m-%Y %H:%M:%S'
    WEBSERVICE_DTF = '%d/%m/%Y %H:%M:%S'

    name = fields.Char('Name')
    entity = fields.Char('Entity', size=5)
    subentity = fields.Char('Subentity', size=3)
    anti_phishing_key = fields.Char('Anti-Phishing Key')
    backoffice_key = fields.Char('Backoffice Key', size=19)
    company_id = fields.Many2one('res.company', string='Company', index=True,
                                 default=lambda self: self.env.user.company_id,
                                 help='Company related to this account IfThenPay')
    journal_id = fields.Many2one('account.journal', string='Journal',
                                 default=lambda self: self.env.ref(
                                     'tko_ifthenpay.journal_ifthenpay_1', raise_if_not_found=False),
                                 help='Journal related to this account IfThenPay')  # has the currency

    _sql_constraints = [
        ('name_uniq', 'unique(name)', _('Name must be unique!')),
    ]

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        journal_copy = self.journal_id.copy()
        default = dict(default or {}, name=_('%s (copy)') % self.name, journal_id=journal_copy.id)
        return super(AccountIfthenpay, self).copy(default)

    @api.model
    def create(self, vals):
        res = super(AccountIfthenpay, self).create(vals)
        ifthenpay_property = self.env['ir.property'].search([('name', '=', 'property_account_ifthenpay_id')])
        if not ifthenpay_property.exists():
            partner_model = self.env['ir.model'].search([('model', '=', 'res.partner')])
            partner_property_field = self.env['ir.model.fields'].search([
                ('name', '=', 'property_account_ifthenpay_id'),
                ('model_id', '=', partner_model.id),
            ])
            ifthenpay_property = self.env['ir.property'].create({
                'name': 'property_account_ifthenpay_id',
                'fields_id': partner_property_field.id,
                'type': 'many2one',
                'value_reference': '%s,%s' % (res._name, res.id)
            })
        return res

    @api.multi
    def generate_multibanco_reference(self, order_id, order_value):
        self.ensure_one()

        ent_id = self.entity
        subent_id = self.subentity

        chk_val = 0
        order_id = '0000' + str(order_id)

        if len(ent_id) != 5:
            raise Warning('Must provide valid Multibanco Entity.')
        if len(subent_id) == 0:
            raise Warning('Must provide valid Multibanco Subentity.')
        if order_value < 1:
            raise Warning('Must provide amount exceeding 1 Euro to generate Multibanco reference.')

        order_value = '%01.2f' % order_value

        if len(subent_id) == 1:
            chk_str = ent_id + subent_id + order_id[-6:] + str('%08.0f' % int(round(float(order_value) * 100)))
        elif len(subent_id) == 2:
            chk_str = ent_id + subent_id + order_id[-5:] + str('%08.0f' % int(round(float(order_value) * 100)))
        else:
            chk_str = ent_id + subent_id + order_id[-4:] + str('%08.0f' % int(round(float(order_value) * 100)))

        chk_array = [3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51]
        i = len(chk_str)
        for chk_item in chk_array:
            chk_val += (int(chk_str[i - 1]) % 10) * chk_item
            i -= 1

        chk_val %= 97
        chk_digits = '%02.0f' % (98 - chk_val)
        reference = '%s%s%s' % (chk_str[5:5 + 3], chk_str[8:8 + 3], chk_str[11:11 + 1] + chk_digits)
        return reference

    def _to_float(self, value_string):
        if ',' in value_string:
            value_string = value_string.replace(',', '.')
        return float(value_string)

    def _to_utc_datetime(self, format, date_time_string=None):
        utc_date_time = None
        if date_time_string is not None:
            date_time = datetime.datetime.strptime(date_time_string, format)
            utc_date_time = timezone_LX.localize(date_time, is_dst=None).astimezone(pytz.utc)
        return utc_date_time

    def _to_lx_datetime(self, date_time):
        return pytz.utc.localize(date_time, is_dst=None).astimezone(timezone_LX)

    @api.model
    def _default_start_utc_datetime(self):
        now = fields.Datetime.now()
        date_lower = now.date().replace(day=1) - datetime.timedelta(days=1)
        lx_datetime_lower = datetime.datetime.combine(date_lower, datetime.time(20, 0, 1))
        datetime_lower = timezone_LX.localize(lx_datetime_lower, is_dst=None).astimezone(pytz.utc)
        return datetime_lower.replace(tzinfo=None)

    def get_webservice_payments(self, payload):
        response = requests.post(WebServiceMultibancoURL, data=payload)
        if response.status_code == 200:
            text = xml.etree.ElementTree.tostring(
                xml.etree.ElementTree.fromstring(response.text), encoding='unicode', method='text')
            response_dicts = json.loads(text)
            errors_exist = next((item for item in response_dicts if item['CodigoErro'] != '0'), False)
            if not errors_exist:
                return response_dicts
            elif len(response_dicts) == 1 and errors_exist['MensagemErro'] == 'Não existem pagamentos.':
                return []
            else:
                raise AccessError('IfThenPay Webservice Error: "%s"' % errors_exist['MensagemErro'])
        else:
            raise AccessError('IfThenPay Webservice Error: "%s"' % response.text)

    @api.multi
    def sync_payments(self, **kwargs):
        self.ensure_one()
        start_utc_datetime = kwargs.get('start_utc_datetime', self._default_start_utc_datetime())
        end_utc_datetime = kwargs.get('end_utc_datetime', fields.Datetime.now())
        webservice_vals = {
            'chavebackoffice': self.backoffice_key,
            'entidade': self.entity,
            'subentidade': self.subentity,
            'dtHrInicio': self._to_lx_datetime(start_utc_datetime).strftime(self.CALLBACK_DTF),
            'dtHrFim': self._to_lx_datetime(end_utc_datetime).strftime(self.CALLBACK_DTF),
            'referencia': '',
            'valor': '',
            'sandbox': 0,
        }
        webservice_payments = self.get_webservice_payments(webservice_vals)
        payment_ids = []
        for item in webservice_payments:
            amount = self._to_float(item['Valor'])
            utc_datetime = self._to_utc_datetime(self.WEBSERVICE_DTF, item['DtHrPagamento'])
            payment_vals = {
                'journal_id': self.journal_id.id,
                'multibanco_entity': item['Entidade'],
                'multibanco_reference': item['Referencia'],
                'amount': amount,
                'multibanco_datetime': utc_datetime,
                'multibanco_terminal': item['Terminal'],
            }
            payment = self.env['account.payment'].process_multibanco_payment(**payment_vals)
            payment_ids.extend(payment.ids)
        return payment_ids
