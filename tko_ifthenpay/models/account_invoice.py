from odoo import api, exceptions, fields, models, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    multibanco_entity = fields.Char('MB Entity', size=5, readonly=True, copy=False)
    multibanco_reference = fields.Char('MB Reference', size=9, readonly=True, index=True, copy=False)

    @api.multi
    def invoice_validate(self):
        eur = self.env.ref('base.EUR')
        # Generate Multibanco references only for Customer Invoices in EUR currency
        condition = lambda invoice: invoice.type == 'out_invoice' and invoice.currency_id.id == eur.id
        for invoice in self.filtered(condition):
            account_ifthenpay = self.partner_id.property_account_ifthenpay_id
            ifthenpay_journal = account_ifthenpay.journal_id
            if not account_ifthenpay or not ifthenpay_journal or not ifthenpay_journal.currency_id:
                raise UserError('No account journal or journal currency is found for current IfThenPay account.')
            multibanco_reference = account_ifthenpay.generate_multibanco_reference(self.id, self.amount_total)
            invoice.write({
                'multibanco_entity': account_ifthenpay.entity,
                'multibanco_reference': multibanco_reference,
            })
        res = super(AccountInvoice, self).invoice_validate()
        return res

    def select_invoice_button(self):
        payment_id = self.env.context.get('payment_id')
        if payment_id:
            payment = self.env['account.payment'].browse(payment_id)
            payment.write({
                'invoice_ids': [(4, self.id)],
                'partner_id': self.partner_id.id,
                'communication': self.reference,
                'has_multibanco_error': False,
            })
            payment.post()
            msg = 'For Multibanco reference %s successfully registered payment %s of invoice %s.' % (
                self.multibanco_reference, payment.name, self.number)
            payment.message_post(body=_(msg))
            self.message_post(body=_(msg))
            return True
