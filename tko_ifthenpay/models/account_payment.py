import logging

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DT


_logger = logging.getLogger(__name__)


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    multibanco_entity = fields.Char('MB Entity', size=5, readonly=True, copy=False)
    multibanco_reference = fields.Char('MB Reference', size=9, readonly=True, index=True, copy=False)
    multibanco_datetime = fields.Datetime('MB Date and Time', readonly=True, copy=False)
    multibanco_terminal = fields.Char('MB Terminal', readonly=True, copy=False)

    has_multibanco_error = fields.Boolean('MB Payment Error', default=False, copy=False)
    multi_invoice_ids = fields.Many2many('account.invoice', 'multi_invoice_payment_rel',
                                         'payment_id', 'invoice_id',
                                         string='Multiple Invoices', copy=False, readonly=True)
    show_multi_invoices = fields.Boolean('Show Multiple Invoices', default=False, copy=False,
                                         compute='_compute_show_multi_invoices', store=False)

    @api.multi
    @api.depends('state', 'multi_invoice_ids')
    def _compute_show_multi_invoices(self):
        for record in self:
            if record.state == 'draft' and record.multi_invoice_ids:
                self.show_multi_invoices = True

    def _prepare_multibanco_payment_vals(self, vals):
        journal = self.env['account.journal'].browse(vals['journal_id'])
        payment_methods = journal.inbound_payment_method_ids
        if not payment_methods.exists():
            e = 'Missing Payment Method Type in %s Journal to create Multibanco Payment.\n' \
                'Please go to Invoicing/Configuration/Accounting/Journals and ' \
                'for %s in Advanced Settings under Payment Method Types ' \
                'select at least one method for For Incoming Payments.' % (journal.name, journal.name)
            _logger.error(e)
            raise UserError(_(e))
        payment_method = payment_methods[0]
        partner_type = 'customer'
        payment_date = vals['multibanco_datetime'].strftime(DT)
        payment_vals = {
            **vals,
            'currency_id': journal.currency_id.id,
            'payment_type': payment_method.payment_type,
            'payment_method_id': payment_method.id,
            'partner_type': partner_type,
            'payment_date': payment_date,
        }
        return payment_vals

    def post_multibanco_payment(self, multibanco_reference, amount):
        # Find invoice
        invoice_domain = [
            ('multibanco_reference', '=', multibanco_reference),
            ('state', '=', 'open'),
            ('amount_total', '=', amount)
        ]
        invoice = self.env['account.invoice'].search(invoice_domain)
        if invoice.exists():
            if len(invoice) == 1:
                self.write({
                    'partner_id': invoice.partner_id.id,
                    'communication': invoice.reference,
                    'invoice_ids': [(4, invoice.id)],
                })
                self.post()
                msg = 'For Multibanco reference %s successfully registered payment %s of invoice %s.' % (
                    self.multibanco_reference, self.name, invoice.number)
                invoice.message_post(body=_(msg))
            else:
                invoices = invoice
                self.write({
                    'has_multibanco_error': True,
                    'multi_invoice_ids': [(6, 0, invoices.mapped('id'))]
                })
                msg = 'For Multibanco reference %s found invoices %s. Please select the right one.' % (
                    self.multibanco_reference, ', '.join(invoices.mapped('number')))
                for invoice in invoices:
                    invoice.message_post(body=_(msg))
        else:
            self.has_multibanco_error = True
            msg = 'For Multibanco reference %s found no invoice.' % self.multibanco_reference
        self.message_post(body=_(msg))
        return True

    @api.model
    def process_multibanco_payment(self, **kwargs):
        payment_search_fields = ['journal_id', 'multibanco_entity', 'multibanco_reference', 'amount', 'multibanco_datetime']
        search_domain = []
        for item in payment_search_fields:
            search_domain.append((item, '=', kwargs[item]))
        payment = self.search(search_domain)
        if not payment.exists():
            payment_vals = self._prepare_multibanco_payment_vals(kwargs)
            payment = self.create(payment_vals)
            payment.post_multibanco_payment(kwargs['multibanco_reference'], kwargs['amount'])
        return payment

    @api.multi
    def post(self):
        for record in self:
            if record.has_multibanco_error and len(record.multi_invoice_ids):
                e = 'The payment cannot be processed because it has unresolved Multiple Invoices.\n' \
                    'Please select and confirm the correct invoice associated with the payment.'
                raise ValidationError(_(e))
        res = super(AccountPayment, self).post()
        return res
