from odoo import api, exceptions, fields, models, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    property_account_ifthenpay_id = fields.Many2one('account.ifthenpay',
                                                    string='Account IfThenPay',
                                                    required=True,
                                                    company_dependent=True,
                                                    help='This account IfThenPay will be used.')
