.. image:: https://img.shields.io/badge/license-LGPL--3-blue.png
   :target: https://www.gnu.org/licenses/lgpl
   :alt: License: LGPL-3

==================================
Multibanco Payments with IfThenPay
==================================

This module extends the functionality of Invoice Management to support Multibanco payments
and allows you to set up the credentials of your IfThenPay account, generate Multibanco references for your invoices
and collect corresponding payments in real-time. Also the invoice document and email templates are adjusted to reflect
all the necessary Multibanco data.

Configuration
=============

To configure this module, you need to obtain and fill in your `IfThenPay <https://ifthenpay.com/>`_
account credentials, such as entity, subentity, anti-phishing key and backoffice key.

.. image:: https://tkopen.com/web/image/14317/tko_ifthenpay_account_ifthenpay.png
   :width: 50%
   :alt: Account IfThenPay
   :target: https://tkopen.com

Description
-----------

Your invoices contain all the necessary Multibanco information.

.. image:: https://tkopen.com/web/image/14319/tko_ifthenpay_invoice_form.png
   :width: 50%
   :alt: Invoice Form
   :target: https://tkopen.com
   
And it is immediately imbedded into communication with the customer.

.. image:: https://tkopen.com/web/image/14318/tko_ifthenpay_email_template.png
   :width: 50%
   :alt: Email Template
   :target: https://tkopen.com
   
.. image:: https://tkopen.com/web/image/14321/tko_ifthenpay_invoice_pdf.png
   :width: 50%
   :alt: Invoice PDF
   :target: https://tkopen.com
   
You receive Multibanco payments in real-time with complete IfthenPay information.

.. image:: https://tkopen.com/web/image/14322/tko_ifthenpay_payment_posted.png
   :width: 50%
   :alt: Payment Received
   :target: https://tkopen.com

And your invoices reflect the paid state with the corresponding payment.

.. image:: https://tkopen.com/web/image/14320/tko_ifthenpay_invoice_paid.png
   :width: 50%
   :alt: Invoice Paid
   :target: https://tkopen.com

Credits
=======

Contributors
------------

* Carlos Almeida <carlos.almeida@tkopen.com>
* Kseniya Bout <kseniya.bout@tkopen.com>

Maintainer
----------

.. image:: https://tkopen.com/web/image/1128
   :width: 25%
   :alt: TKOpen
   :target: https://tkopen.com
