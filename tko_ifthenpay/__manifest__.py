# © 2019 TKOpen <https://tkopen.com>
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    'name': 'Multibanco Payments with IfThenPay',
    'summary': 'Generate Multibanco references and collect corresponding payments real-time',
    'author': 'TKOpen',
    'category': 'Accounting',
    'license': 'AGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.1.0.0',
    'sequence': 1,
    'depends': [
        'account',
    ],
    'data': [
        'data/account_journal.xml',
        'data/account_ifthenpay.xml',
        'data/mail_template_data.xml',
        'security/ir.model.access.csv',
        'wizard/account_ifthenpay_wizard.xml',
        'views/account_ifthenpay_view.xml',
        'views/account_invoice_view.xml',
        'views/account_payment_view.xml',
        'views/res_partner_view.xml',
        'views/report_invoice.xml',
    ],
    'images': [''],
    'installable': True,
    'application': False,
    'auto_install': False,
    'price': 120,
    'currency': 'EUR',

}
