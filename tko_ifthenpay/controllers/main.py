import logging

import odoo.http as http
from odoo.http import request
from odoo.exceptions import MissingError, ValidationError


_logger = logging.getLogger(__name__)


MULTIBANCO_CALLBACK_PARAMETERS = ['chave', 'entidade', 'referencia', 'valor']


class IfthenpayCallback(http.Controller):

    def _check_required_params(self, params, vals):
        error = ''
        for item in params:
            if item not in vals:
                error += '%s\n' % item
        if error:
            error = 'Missing required parameters:\n' + error
            raise ValidationError(error)
        vals = {k: v.strip('[]') for k, v in vals.items()}
        return vals

    def _get_account_ifthenpay(self, key, entity):
        account_ifthenpay = request.env['account.ifthenpay'].sudo().search([('anti_phishing_key', '=', key),('entity', '=', entity)])
        document_sudo = account_ifthenpay.sudo().exists()
        if not document_sudo:
            raise MissingError('The Account IfThenPay with Anti-Phishing Key %s does not exist.' % key)
        elif len(document_sudo) != 1:
            raise ValidationError('Anti-Phishing Key %s belongs to more than one Account IfThenPay.' % key)
        return document_sudo

    @http.route('/ifthenpay/v1/multibanco', type='http', auth='none', csrf=False)
    def receive_multibanco_payment(self, **kwargs):  # chave, entidade, referencia, valor, datahorapag, terminal
        _logger.info('Received Call Back Multibanco with params: %s' % str(kwargs))
        try:
            vals = self._check_required_params(MULTIBANCO_CALLBACK_PARAMETERS, kwargs)
            account_ifthenpay = self._get_account_ifthenpay(vals['chave'], vals['entidade'])
            if account_ifthenpay.entity == vals['entidade']:
                amount = account_ifthenpay._to_float(vals['valor'])
                utc_datetime = account_ifthenpay._to_utc_datetime(account_ifthenpay.CALLBACK_DTF,
                                                                  vals.get('datahorapag'))
                payment_vals = {
                    'journal_id': account_ifthenpay.journal_id.id,
                    'multibanco_entity': vals['entidade'],
                    'multibanco_reference': vals['referencia'],
                    'amount': amount,
                    'multibanco_datetime': utc_datetime,
                    'multibanco_terminal': vals.get('terminal'),
                }
                payment = request.env['account.payment'].sudo().process_multibanco_payment(**payment_vals)
                return http.Response('OK', status=200)
            else:
                raise ValidationError('The Account IfThenPay with id %d does not have Entity %s.' %
                                      account_ifthenpay.id, vals['entidade'])
        except Exception as e:
            return http.Response(str(e), status=500)
